<?php 
session_start(); 
if(empty($_SESSION['usuario'])){
    header('Location: login');
    exit();
}
include_once '../modelos/api.php';
include_once '../include/toarst.php'; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Eventos</title>
</head>
<body>
<form action="cadastrarMateria" method="POST">
                        <input type="text" name="nome" placeholder="Nome do Evento">
                        <input type="date" name="dataentrega" placeholder="Data Entrega">
                        <select name="tipoevento" id="" placeholder="Tipo Evento">
                            <option value="" disabled selected>Evento</option>
                            <?php 
                                $api = new TipoEventos();
                                $cadastroeventos = $api->listar();
                                // Repetição para mostar status
                                foreach ($cadastroeventos as $valor){
                                    $nome = $valor['nome'];
                                    $id = $valor['id'];
                                    echo '<option value="'.$id.'">'.$nome.'</option>';
                                }
                            ?>
                        </select>
                        <select name="materiaId" id="" placeholder="Materia Id">
                            <option value="" disabled selected>MateriaID</option>
                            <?php 
                                $api = new Materia();
                                $cadastroeventos = $api->listar($_SESSION['usuarios_id']);

                                // Repetição para mostar status
                                foreach ($cadastroeventos as $valor){
                                    $nome = $valor['nome'];
                                    $id = $valor['id'];
                                    echo '<option value="'.$id.'">'.$nome.'</option>';
                                }
                            ?>
                        </select>

                        <input type="text" name="descricao" placeholder="Descrição">
                        <button>Salvar</button>
                    </form>
</body>
</html>