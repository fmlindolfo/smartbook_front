<?php 
session_start(); 
if(empty($_SESSION['usuario'])){
    header('Location: login');
    exit();
}
include_once '../modelos/api.php';
include_once '../include/toarst.php'; 
?>

<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/sistema.css">
    <link rel="icon" href="favicon.ico">
    <title>Painel</title>
</head>
<body>

    <div class="popup__materia desativado">
        <div class="container__materia">
            <div class="fundo__popup--vanilla">
                <div class="container__top">
                    <div class="fundo__top--popup">
                        <div></div>
                        <div class="popup__nome">Matéria</div>
                        <div class="close__popup">X</div>
                    </div>
                </div>
                <div class="container__meio">
                    <form action="cadastrarMateria" method="POST">
                        <input type="text" name="nome" placeholder="Nome da Matéria">
                        <input type="date" name="datainicio" placeholder="Data Inicio">
                        <select name="statusmateria" id="" placeholder="Status Materia">
                            <option value="" disabled selected>Status da Matéria</option>
                            <?php 
                                $api = new StatusMateria();
                                $statusmateria = $api->listar();

                                // Repetição para mostar status
                                foreach ($statusmateria as $valor){
                                    $nome = $valor['nome'];
                                    $id = $valor['id'];
                                    echo '<option value="'.$id.'">'.$nome.'</option>';
                                }
                            ?>
                        </select>
                        <input type="text" name="descricao" placeholder="Descrição">
                        <button>Salvar</button>
                    </form>
                </div>
                <div class="container__footer"></div>
            </div>
        </div>
    </div>

    <div class="popup__materia">
        <div class="container__materia">
            <div class="fundo__popup--vanilla">
                <div class="container__top">
                    <div class="fundo__top--popup">
                        <div></div>
                        <div class="popup__nome">Delete Matéria</div>
                        <div class="close__popup">X</div>
                    </div>
                </div>
                <div class="container__meio">
                    <form action="" method="POST">
                        <input type="text" name="nome" placeholder="Nome" disabled>
                        <button>Salvar</button>
                    </form>
                </div>
                <div class="container__footer">
                    
                </div>
            </div>
        </div>
    </div>




    <div class="navbar__esquerda"></div>
    <div class="navbar__superior">
        <div class="perfil">
            <div class="nome__perfil">
                <p class="nome"><?php echo $_SESSION['primeironome']; ?></p>
                <p class="curso"></p>
            </div>
            <div class="icon__perfil"><a href="logout">></a></i></div>
        </div>
    </div>

    <div class="main__container">
        <div class="titulo__container">
            <div class="tiutlo__materia">
                <p>Matérias</p>
            </div>
            <div class="tiutlo__evento">
                <p class="evento__ativo">Atividades</p>
                <p class="">Provas</p>
            </div>
        </div>
        <div class="funcao__container">
            <div class="funcao__meteria">
                <div class="add__materia funcao__btn">+</div>
            </div>
            <div class="funcao__evento">
                <div class="funcao__atividade">
                    <div class="comando">
                        <div class="add__evento funcao__btn">+</div>
                    </div>
                    <div class="salvar"> 
                        <div class="filtro__salvar">Salvar</div>
                    </div>
                </div>
                <div class="funcao__provas">Anotações</div>
            </div>
        </div>
        <div class="dado__containter">

            <div class="dado__materia">
                <?php
                    $api = new Materia();
                    $materias = $api->listar($_SESSION['usuarios_id']);

                    // Repetição para mostar materias
                    foreach ($materias as $valor){
                        $nome = $valor['nome'];
                        $descricao = $valor['descricao'];
                        $id = $valor['id'];


                        echo '  <div class="materia" id="'.$id.'">
                                    <div class="materia__principal">
                                        <p>'.$nome.'</p>
                                        <span>'.$descricao.'</span>
                                        <div class="funcoes">
                                            <button class="delete__btn">Delete</button>
                                            <button class="editar__btn">Editar</button>
                                        </div>
                                    </div>
                                </div>';
                    }
                    
                ?>
            </div>

            <div class="dado__evento">
                <div class="evento">
                    <div class="principal__evento">
                        <div class="primerio__dado">
                            <p>Código GitLab</p>
                            <span>Linguagem de Programação</span>
                            <span>02/Mai</span>
                        </div>
                        <div class="segundo__dado atencao">
                            <input class="toggle-switch" type="checkbox" name="" id="">
                            <p>Andamento</p>
                        </div>
                    </div>
                    <div class="princial__descricao">
                        <p>Felipe Mendes Lindolfo</p>
                        <p>02/Mai</p>
                    </div>
                </div>
                <div class="evento">
                    <div class="principal__evento">
                        <div class="primerio__dado">
                            <p>Documentação</p>
                            <span>Linguagem de Programação</span>
                            <span>02/Mai</span>
                        </div>
                        <div class="segundo__dado perigo">
                            <input class="toggle-switch" type="checkbox" name="" id="">
                            <p>Parada</p>
                        </div>
                    </div>
                    <div class="princial__descricao"></div>
                </div>
                <div class="evento">
                    <div class="principal__evento">
                        <div class="primerio__dado">
                            <p>Código GitLab</p>
                            <span>Linguagem de Programação</span>
                            <span>02/Mai</span>
                        </div>
                        <div class="segundo__dado sucesso">
                            <input class="toggle-switch" type="checkbox" name="" id="">
                            <p>Concluído</p>
                        </div>
                    </div>
                    <div class="princial__descricao"></div>
                </div>

            </div>
        </div>
    </div>
</body>

<?php 
if (!empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
<script src="js/popup.js"></script>
</html>