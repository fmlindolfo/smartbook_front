<?php
session_start();
include_once '../modelos/api.php';
include_once '../include/toarst.php';

if(empty($_POST['nome']) || empty($_SESSION['usuarios_id']) || empty($_POST['datainicio'])  || empty($_POST['descricao'])){
    header('Location: painel');
    exit();
};

//|| empty($_POST['statusmateria'])

$nome = $_POST['nome'];
$usuario = $_SESSION['usuarios_id'];
$dataincio = $_POST['datainicio'];
$descricao = $_POST['descricao'];
$statusmateria = 1;

$api = new Materia();
$returnoCodigo = $api->cadastrar($nome, $usuario, $dataincio, $statusmateria, $descricao);

echo $returnoCodigo;

if($returnoCodigo == 411){
    $mensagem = 'Mínimo de caracter não atingido.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.warning("'.$mensagem.'")</script>';
    header('Location: painel');

} else if ($returnoCodigo == 406){
    $mensagem = 'Matéria não vinculada.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.error("'.$mensagem.'")</script>';
    header('Location: painel');

} else if ($returnoCodigo == 201){
    $mensagem = 'Máteria cadastrado com sucesso.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.success("'.$mensagem.'")</script>';
    header('Location: painel');
}
