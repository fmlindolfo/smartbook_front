<?php 
session_start();
if(!empty($_SESSION['usuario'])){
    session_destroy();
    header('Location: login');
    exit();
} else {
    header('Location: painel');
}
?>