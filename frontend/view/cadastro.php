
<?php 
session_start();
include_once '../include/toarst.php'; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro</title>
    <link rel="stylesheet" href="css/cadastro.css">
    <link rel="icon" href="favicon.ico">
</head>
<body>
    <div class="main">
        <div class="form">
            <form action="cadastrarUsuario" method="POST">
                <input type="text" name="nome" placeholder="Nome Completo" required>
                <input type="text" name="usuario" placeholder="Usuário" required>
                <input type="text" name="email" placeholder="E-mail" required>
                <input type="text" name="cpf" placeholder="CPF" required>
                <input type="password" name="senha" placeholder="Senha" required>
                <input type="password"  name="Csenha" placeholder="Confirmar Senha" required>
                <button>Cadastrar</button>
            </form>
        </div>

        <div class="logo">
            <a href="home"><img src="img/sofa2.png" alt="" class="logo__cadastro"></a>
        </div>
    </div>
</body>

<?php 
if (!empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>

</html>