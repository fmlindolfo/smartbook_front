<?php 
session_start();
include_once '../include/toarst.php'; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="icon" href="favicon.ico">
</head>
<body>
    <div class="tela">
        <div class="info">
            <div class="form">
                <form action="validaLogin" method="POST">
                    <input type="text" name="usuario" placeholder="Usuario" required>
                    <input type="password" name="senha" placeholder="Senha" required>
                    <button>Login</button>
                </form>
            </div>

            <div class="logo">
                <a href="home"><img src="img/tela1_smart.png" alt=""></a>
            </div>
        </div>
    </div>
</body>

<?php 
if (!empty($_SESSION['msg'])){
    echo $_SESSION['msg'];
    unset($_SESSION['msg']);
}
?>
</html>