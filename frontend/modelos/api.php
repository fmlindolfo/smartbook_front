<?php 

class Materia {

    public function listar($usuarios_id){
        $opcoes = array('http' =>
            array(
                'method'  => 'GET',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => ''
            )
        );

        $contexto = stream_context_create($opcoes);
        $consulta   = file_get_contents('http://localhost:8000/api/materia?usuarios_id='.$usuarios_id, false, $contexto);
        $resultado = json_decode($consulta, TRUE);

        if($resultado > 0){
            return $resultado['materia'];
        } else {
            return 0;
        }
    }

    public function cadastrar ($nome, $usuario, $dataincio, $statusmateria, $descricao){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'localhost:8000/api/materia',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "usuarios_id": '.$usuario.',
                "nome":"'.$nome.'",
                "descricao":"'.$descricao.'",
                "data_inicio":"'.$dataincio.'",
                "status_materias_id": '.$statusmateria.'
            }',
            CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;
    }
}

class Usuario {
    public function cadastrar ($nome, $cpf, $usuario, $senha, $email){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'localhost:8000/api/usuario',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "nome":"'.$nome.'",
                "cpf":"'.$cpf.'",
                "usuario":"'.$usuario.'",
                "senha":"'.$senha.'",
                "email":"'.$email.'"
            }',
            CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
        ));

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $httpcode;
    }
}

class StatusMateria {

    public function listar (){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'localhost:8000/api/statusmateria',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $resultado = curl_exec($curl);
        $consulta = json_decode($resultado, TRUE);

        if($consulta > 0){
            return $consulta['status_materia'];
        } else {
            return 0;
        }
    }
}