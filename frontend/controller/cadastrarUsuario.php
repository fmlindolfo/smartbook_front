<?php
session_start();
include_once '../modelos/api.php';
include_once '../include/toarst.php';

if(empty($_POST['nome']) || empty($_POST['usuario']) || empty($_POST['email']) || empty($_POST['cpf']) || empty($_POST['senha'])){
    header('Location: cadastro');
    exit();
};

$nome = $_POST['nome'];
$usuario = $_POST['usuario'];
$email = $_POST['email'];
$cpf = $_POST['cpf'];
$senha = $_POST['senha'];

$api = new Usuario();
$returnoCodigo = $api->cadastrar($nome, $cpf, $usuario, $senha, $email);

if($returnoCodigo == 411){
    $mensagem = 'Mínimo de caracter não atingido.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.warning("'.$mensagem.'")</script>';
    header('Location: cadastro');

} else if ($returnoCodigo == 406){
    $mensagem = 'Usário já cadastrado.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.error("'.$mensagem.'")</script>';
    header('Location: cadastro');

} else if ($returnoCodigo == 201){
    $mensagem = 'Usúario cadastrado com sucesso.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.success("'.$mensagem.'")</script>';
    header('Location: login');
}