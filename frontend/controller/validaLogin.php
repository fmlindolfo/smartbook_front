<?php
session_start();
include('../modelos/conexao.php');

if(empty($_POST['usuario']) || empty($_POST['senha'])){

    $mensagem = 'Preencha todos os campos.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.warning("'.$mensagem.'")</script>';

    header('Location: login');
    exit();
};

$usuario = ($_POST['usuario']);
$senha = ($_POST['senha']);

$query = "SELECT *, SUBSTRING_INDEX(SUBSTRING_INDEX(nome, ' ', 1), ' ', -1)  AS primeironome
            FROM usuarios 

            WHERE usuario = '{$usuario}' 
            AND senha = '{$senha}'";

$resultado = mysqli_query($conexao, $query);
$linha = mysqli_fetch_all($resultado);
$row = mysqli_num_rows($resultado);

if ($row == 1){
    $_SESSION['usuario'] = $usuario;
    $_SESSION['usuarios_id'] = $linha[0][0];
    $_SESSION['primeironome'] = $linha[0][8];

    header('Location: painel');
} else  {

    $mensagem = 'Usúario ou senha inválidos.';
    $_SESSION['msg'] = '<script type="text/javascript">toastr.error("'.$mensagem.'")</script>';

    header('Location: login');
}

