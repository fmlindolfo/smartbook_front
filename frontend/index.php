<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="icon" href="favicon.ico">
    <title>Home</title>
</head>
<body>
    <div class="container">
        <nav>
            <ul>
                <li><img src="img/home.png" alt=""></li>
                <li class="entrada">
                    <a href="cadastro"><button class="cadastro">Cadastre-se</button></a>
                    <a href="login"><p class="login">Login</p></a>
                </li>  
            </ul>
        </nav>

        <div class="bloco__principal">
            <img class="flutuante" src="img/coisas.png" alt="">
            <div class="bloco__logo">
                <img src="img/logo.png" alt="" class="logo">
                <button class="planos">Planos</button>
            </div>
            <img src="img/homem.png" alt="" class="homem">
        </div>

        <div class="redes">
            <img src="img/discord.png" alt="" class="discord">
            <img src="img/twitter.png" alt="" class="twitter">
            <img src="img/twitch.png" alt="" class="twitch">
            <img src="img/youtube.png" alt="" class="youtube">
        </div>
    </div>

        

</body>
</html>

